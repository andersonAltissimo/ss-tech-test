<?php

namespace App\Http\Entities;

use FacilityTest;
use PHPUnit\Framework\TestCase;

class FacilityTest extends TestCase
{
    function testCreateFacilityEntity()
    {
        $facility = new Facility();

        $facility->setId(1);
        $facility->setLocationCode(2);
        $facility->setName('Anderson Altissimo');
        $facility->setStreetAddress('292 Nivaldo Nessi Street');
        $facility->setCity('Iguassu Falls');
        $facility->setState('Parana');
        $facility->setCounty('New City');
        $facility->setPostalCode('85870-719');
        $facility->setLatitude(123);
        $facility->setLongitude(321);
        $facility->setPhone('+554599329559');
        $facility->setContactEmail('ander.altissimo@gmail.com');
        $facility->setBreGroupId(10);

        $this->assertEquals(1, $facility->getId());
        $this->assertEquals(2, $facility->getLocationCode());
        $this->assertEquals('Anderson Altissimo', $facility->getName());
        $this->assertEquals('292 Nivaldo Nessi Street', $facility->getStreetAddress());
        $this->assertEquals('Iguassu Falls', $facility->getCity());
        $this->assertEquals('Parana', $facility->getState());
        $this->assertEquals('New City', $facility->getCounty());
        $this->assertEquals('85870-719', $facility->getPostalCode());
        $this->assertEquals(123, $facility->getLatitude());
        $this->assertEquals(321, $facility->getLongitude());
        $this->assertEquals('+554599329559', $facility->getPhone());
        $this->assertEquals('ander.altissimo@gmail.com', $facility->getContactEmail());
        $this->assertEquals(10, $facility->getBreGroupId());
    }

    function testFormatCounty()
    {
        $facility = new Facility();
        $this->assertEquals('', $facility->getFormattedCounty());

        $facility->setCounty('New City');
        $this->assertEquals('', $facility->getFormattedCounty());


        $facility->setState('Parana');
        $this->assertEquals("New City County", $facility->getFormattedCounty());

        $facility->setState('AK');
        $this->assertEquals("New City Borough", $facility->getFormattedCounty());

        $facility->setState('LA');
        $this->assertEquals("New City Parish", $facility->getFormattedCounty());
    }


}
