<?php

namespace App\Repositories;

use App\Models\Payment;

class PaymentRepository
{
    public function getAll(array $reqOptions): ?array
    {
        $limit = $reqOptions['limit'] ?? 20;

        $payments = Payment::query()
            ->with(['facility', 'user'])
            ->limit($limit)
            ->orderBy('id', $reqOptions['sortDir'] === 'desc' ? 'desc' : 'asc')
            ->get()
            ->toArray();

        return $payments;
    }
}
