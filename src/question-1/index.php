<?php

function reverseArray($array) {
    $arrayLength = count($array);
     
    $reverseArray = [];
    for ($i=$arrayLength-1; $i >= 0 ; $i--) { 
        $reverseArray[] = $array[$i];   
    }
    
    return $reverseArray;
}

$firstArray = reverseArray([1,2,3,4,5]);

$secondArray = reverseArray(['Apple', 'Banana', 'Orange', 'Coconut']);

echo print_r($firstArray);
echo print_r($secondArray);

?>